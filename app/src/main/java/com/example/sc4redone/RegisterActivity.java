package com.example.sc4redone;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    TextView login;

    EditText edtNama, edtEmail, edtPswd;

    Button btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        login = findViewById(R.id.txtLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToLogin =  new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(goToLogin);
                finish();
            }
        });

        edtEmail = findViewById(R.id.EmailReg);
        edtPswd = findViewById(R.id.PswdReg);

    }
    public void regist(View view){
        if (checker()){
            mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(edtEmail.getText().toString(),edtPswd.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                                finish();
                            }
                        }
                    });
        }
    }




    public boolean checker(){
        if (edtEmail.getText().toString().isEmpty()){
            edtEmail.setError("Name cant be empty");
            edtEmail.requestFocus();
            return false;
        } if (edtPswd.getText().toString().isEmpty()){
            edtPswd.setError("Password cant be empty");
            edtPswd.requestFocus();
            return false;
        }
        return true;
    }
}
